# Hermes

## Exchange messages between different platforms in a pluggable manner

There are basically three concepts around the Hermes project:

- **Origins**: modules to abstract the platform that I will get the messages from
- **Destinations**: modules to abstract the platform that I will deliver the messages to
- **Sandals**: programs that will actually carry the message from one side to the other

The analogy comes from the Greek god Hermes and the fact that he is known as the
messenger of the gods, and normally is depicted using flying sandals.

## Currently supported platforms

### Origins

**Apache Kafka (avro, with Schema Registry)**

Built on top of the official python SDK ([confluent-kafka-python](https://github.com/confluentinc/confluent-kafka-python)) for Apache Kafka.

### Destinations

**AWS Kinesis Data Streams**

Built on top of the official python SDK ([boto3](https://github.com/boto/boto3)) for Amazon Web Services.

## Tests

Make sure you have Docker installed and configured

Start a shell inside `poetry`

```shell
poetry shell
```

Hermes uses [fast-data-dev](https://github.com/lensesio/fast-data-dev) as developing environment
for Apache Kafka. To spin an Apache Kafka cluster ready for development, just run:

```shell
docker run --rm --net=host \
    --env SCHEMA_REGISTRY_KAFKASTORE_TOPIC=_schemas \
    --env KAFKA_REST_SCHEMA_REGISTRY_URL=http://localhost:8081 \
    --env SAMPLEDATA=0 lensesio/fast-data-dev
```

Run [localstack](https://github.com/localstack/localstack) to get an Amazon Web Services emulated
environment (you can change the temporary folders if you want)

```shell
HOST_TMP_FOLDER=${HOME}/.local/localstack/tmp \
DATA_DIR=${HOME}/.local/localstack/data \
ENTRYPOINT=-d SERVICES=dynamodb,kinesis \
LOCALSTACK_HOSTNAME=localhost \
localstack start --docker
```

Run your tests

```shell
poetry run python -m pytest tests
```

## Roadmap

- Tests
- Move configurations to configuration files
- Add more robustness to some modules
- Change some things to do them asynchronously
