consumer_conf = {
    'bootstrap.servers': 'ec2-107-22-80-230.compute-1.amazonaws.com:9092',
    'group.id': 'hermes-group',
    'enable.auto.commit': 'false'
}

sr_conf = {
    'url': 'http://ec2-107-22-80-230.compute-1.amazonaws.com:8081'
}

KAFKA_TOPIC = 'performance-test-hermes-topic'
MAX_BUFFER_SIZE = 50
KINESIS_STREAM = 'performance-test-stream-test-kafka-kinesis'
KAFKA_DYNAMODB_OFFSETS_TABLE = 'performance-test-hermes-topic-offsets'
