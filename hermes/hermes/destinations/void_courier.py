import logging


class VoidCourier:
    """Courier that logs messages to stdout"""

    def __init__(self):
        self.messages = []

    def deliver(self):
        print('Delivering messages:')
        for message in self.messages:
            logging.info(message)
