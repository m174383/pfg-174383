from hermes.config import consumer_conf, sr_conf, KAFKA_TOPIC, KAFKA_DYNAMODB_OFFSETS_TABLE, KINESIS_STREAM
from confluent_kafka import TopicPartition
from origins.kafka.avro_consumer import AvroConsumer
from origins.kafka.offset.offset_persister import OffsetPersister
from destinations.courier import Courier
import logging


# logging.basicConfig(level=logging.INFO)s


def _on_revoke(consumer, partitions):
    pass


def _on_assign(consumer, partitions):
    topic = partitions[0].topic
    partition_numbers = [p.partition for p in partitions]
    op = OffsetPersister('dynamodb', KAFKA_DYNAMODB_OFFSETS_TABLE, region='us-east-1')
    loff = op.get_last_offsets(partition_numbers)
    consumer.assign(
        [TopicPartition(topic, part, offs) for part, offs in loff.items()]
    )
    print(f'New partition assignment: {loff}')


def main():

    offp = OffsetPersister('dynamodb', topic=KAFKA_TOPIC, tbl_name= KAFKA_DYNAMODB_OFFSETS_TABLE, region='us-east-1')
    cour = Courier('kinesis', stream = KINESIS_STREAM, region='us-east-1')

    ac = AvroConsumer(
        sr_conf=sr_conf,
        consumer_conf=consumer_conf,
        topic=KAFKA_TOPIC,
        courier=cour,
        offset_persister=offp,
        value_schema_subject=KAFKA_TOPIC+'-value'
    )

    ac.subscribe(on_assign=_on_assign, on_revoke=_on_revoke)
    ac.consume()


if __name__ == '__main__':
    main()
