import boto3
from confluent_kafka.admin import AdminClient, NewTopic
from confluent_kafka import avro
from confluent_kafka.avro import AvroProducer
import pytest
from time import sleep
from hermes.config import KAFKA_DYNAMODB_OFFSETS_TABLE, \
    KINESIS_STREAM, KAFKA_TOPIC
from hermes.destinations.courier import Courier
from hermes.origins.kafka.offset.offset_persister import OffsetPersister


@pytest.fixture(scope='module')
def create_table(request):

    dev_dynamodb = boto3.resource(
        'dynamodb',
        region_name='us-east-1',
        endpoint_url='http://localhost:4566',
        aws_access_key_id='test',
        aws_secret_access_key='test'
    )

    dev_dynamodb.create_table(
        TableName=KAFKA_DYNAMODB_OFFSETS_TABLE,
        AttributeDefinitions=[
            {
                'AttributeName': 'topic',
                'AttributeType': 'S'
            },
            {
                'AttributeName': 'partition',
                'AttributeType': 'N'
            }
        ],
        KeySchema=[
            {
                'AttributeName': 'topic',
                'KeyType': 'HASH'
            },
            {
                'AttributeName': 'partition',
                'KeyType': 'RANGE'
            }
        ],
        ProvisionedThroughput={
            'ReadCapacityUnits': 5,
            'WriteCapacityUnits': 5
        }
    )

    sleep(2)  # Wait for stream creation

    def delete_table():
        table = dev_dynamodb.Table(KAFKA_DYNAMODB_OFFSETS_TABLE)
        table.delete()
        sleep(2)

    request.addfinalizer(delete_table)


@pytest.fixture(scope='module')
def create_stream(request):

    dev_kinesis = boto3.client(
        'kinesis',
        region_name='us-east-1',
        endpoint_url='http://localhost:4566',
        aws_access_key_id='test',
        aws_secret_access_key='test'
    )

    dev_kinesis.create_stream(
        StreamName=KINESIS_STREAM,
        ShardCount=4
    )
    sleep(2)  # Wait for stream creation

    def delete_stream():
        dev_kinesis.delete_stream(
            StreamName=KINESIS_STREAM,
            EnforceConsumerDeletion=True
        )
        sleep(2)

    request.addfinalizer(delete_stream)


@pytest.fixture(scope='module')
def get_courier():
    dev_kinesis = boto3.client(
        'kinesis',
        region_name='us-east-1',
        endpoint_url='http://localhost:4566',
        aws_access_key_id='test',
        aws_secret_access_key='test'
    )
    c = Courier('kinesis')
    c.courier.client = dev_kinesis
    return c


@pytest.fixture(scope='module')
def get_persister():
    dev_dynamodb = boto3.resource(
        'dynamodb',
        region_name='us-east-1',
        endpoint_url='http://localhost:4566',
        aws_access_key_id='test',
        aws_secret_access_key='test'
    )
    t = dev_dynamodb.Table(KAFKA_DYNAMODB_OFFSETS_TABLE)
    offp = OffsetPersister('dynamodb', KAFKA_TOPIC, region='us-east-1')
    offp.persister.table = t
    return offp


@pytest.fixture(scope='module')
def create_topic():
    a = AdminClient({'bootstrap.servers': 'localhost:9092'})
    new_topic = [
        NewTopic("hermes-topic", num_partitions=20, replication_factor=1)
    ]
    fs = a.create_topics(new_topic)
    for topic, f in fs.items():
        try:
            f.result()
        except Exception:
            pass
    sleep(2)


@pytest.fixture(scope='module')
def publish():
    value_schema_str = """
    {
        "type": "record",
        "name": "topicValue",
        "namespace": "com.landoop.topic",
        "fields": [
            {
            "name": "id",
            "type": "string"
            }
        ]
    }
    """
    value_schema = avro.loads(value_schema_str)
    avroProducer = AvroProducer(
        {
            'bootstrap.servers': 'localhost:9092',
            'on_delivery': lambda e, msg: None,
            'schema.registry.url': 'http://localhost:8081'
        },
        default_value_schema=value_schema
    )
    for i in range(5):
        value = {"id": f"msg-{i}"}
        avroProducer.produce(topic='hermes-topic', value=value)
    avroProducer.flush()


@pytest.fixture(scope='module')
def prepare_topic(create_topic, publish, request):
    def delete_topic():
        a = AdminClient({'bootstrap.servers': 'localhost:9092'})
        topic = ["hermes-topic"]
        fs = a.delete_topics(topic)
        for topic, f in fs.items():
            try:
                f.result()
            except Exception:
                pass

    request.addfinalizer(delete_topic)
