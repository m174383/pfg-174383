import boto3
from boto3.dynamodb.conditions import Key, Attr
from collections import namedtuple
from hermes.config import KAFKA_DYNAMODB_OFFSETS_TABLE
from hermes.origins.kafka.offset.dynamo_db_offset_persister \
    import DynamoDBOffsetPersister
from unittest.mock import patch

Offset = namedtuple('Offset', 'offset persisted')

dev_dynamodb = boto3.resource(
    'dynamodb',
    region_name='us-east-1',
    endpoint_url='http://localhost:4566',
    aws_access_key_id='test',
    aws_secret_access_key='test'
)


def get_dev_table(self, table_name, region):
    return dev_dynamodb.Table(KAFKA_DYNAMODB_OFFSETS_TABLE)


@patch.object(
    DynamoDBOffsetPersister,
    '_get_table',
    get_dev_table
)
def test_persist(create_table):
    my_offsets = {
        0: Offset(7, True),
        1: Offset(42, False)
    }
    persister = DynamoDBOffsetPersister('my-topic')
    persister.persist(my_offsets)

    table = dev_dynamodb.Table(KAFKA_DYNAMODB_OFFSETS_TABLE)
    response = table.scan(
        FilterExpression=Key('topic').eq('my-topic') &
        Attr('partition').is_in([0, 1])
    )
    last_offsets = {
        int(i['partition']): int(i['partition_offset'])
        for i in response['Items']
    }

    assert last_offsets == {1: 42}


@patch.object(
    DynamoDBOffsetPersister,
    '_get_table',
    get_dev_table
)
def test_get_last_offsets(create_table):
    persister = DynamoDBOffsetPersister('my-topic')
    partition_numbers = [0, 1]
    last_offsets = persister.get_last_offsets(
        partition_numbers
    )
    assert last_offsets == {0: 0, 1: 42}
