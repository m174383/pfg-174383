from confluent_kafka.admin import AdminClient, NewTopic
from hermes.config import KAFKA_TOPIC

def create_topic():
    a = AdminClient({'bootstrap.servers': 'ec2-54-175-207-94.compute-1.amazonaws.com:9092'})
    new_topic = [
        NewTopic(KAFKA_TOPIC, num_partitions=1, replication_factor=1)
    ]
    fs = a.create_topics(new_topic)
    for topic, f in fs.items():
        try:
            f.result()
        except Exception as e:
            print(e)

def main(): 
    create_topic()


if __name__ == "__main__":
    main()