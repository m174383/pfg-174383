from confluent_kafka.admin import AdminClient, NewTopic
from hermes.config import KAFKA_TOPIC

def prepare_topic():
    a = AdminClient({'bootstrap.servers': 'ec2-54-175-207-94.compute-1.amazonaws.com:9092'})
    topic = [KAFKA_TOPIC]
    fs = a.delete_topics(topic)
    for topic, f in fs.items():
        try:
            f.result()
        except Exception:
            pass

def main(): 
    prepare_topic()


if __name__ == "__main__":
    main()