from confluent_kafka import avro
from confluent_kafka.avro import AvroProducer
from hermes.config import KAFKA_TOPIC
import time
import uuid
def main():
    value_schema_str = """
    {
        "type": "record",
        "name": "topicValue",
        "namespace": "com.landoop.topic",
        "fields": [
            {
            "name": "id",
            "type": "string"
            },
            {
            "name": "time",
            "type": "string"
            }
        ]
    }
    """
    value_schema = avro.loads(value_schema_str)
    avroProducer = AvroProducer(
        {
            'bootstrap.servers': 'ec2-54-175-207-94.compute-1.amazonaws.com:9092',
            'on_delivery': lambda e, msg: None,
            'schema.registry.url': 'http://ec2-54-175-207-94.compute-1.amazonaws.com:8081',
            'debug': 'broker,admin'
        },
        default_value_schema=value_schema
    )
    while True:
        for _ in range(1000):
            value = {
                "id": str(uuid.uuid4()),
                "time": str(time.time())
            }
            avroProducer.produce(topic=KAFKA_TOPIC, value=value)
        avroProducer.flush()

if __name__ == "__main__":
    main()
