import boto3
from confluent_kafka.admin import AdminClient, NewTopic
from confluent_kafka import avro
from confluent_kafka.avro import AvroProducer
from time import sleep
from hermes.config import KAFKA_DYNAMODB_OFFSETS_TABLE, \
    KINESIS_STREAM, KAFKA_TOPIC
from hermes.destinations.courier import Courier
from hermes.origins.kafka.offset.offset_persister import OffsetPersister
from hermes.origins.kafka.avro_consumer import AvroConsumer
from confluent_kafka import TopicPartition
import multiprocessing
import logging
from datetime import datetime
import uuid


consumer_conf = {
    'bootstrap.servers': '18.229.132.175:9092',
    'group.id': 'hermes-group',
    'enable.auto.commit': 'false'
}

sr_conf = {
    'url': 'http://18.229.132.175:8081'
}

def create_table():

    dev_dynamodb = boto3.resource(
        'dynamodb',
        region_name='us-east-1'
    )

    dev_dynamodb.create_table(
        TableName=KAFKA_DYNAMODB_OFFSETS_TABLE,
        AttributeDefinitions=[
            {
                'AttributeName': 'topic',
                'AttributeType': 'S'
            },
            {
                'AttributeName': 'partition',
                'AttributeType': 'N'
            }
        ],
        KeySchema=[
            {
                'AttributeName': 'topic',
                'KeyType': 'HASH'
            },
            {
                'AttributeName': 'partition',
                'KeyType': 'RANGE'
            }
        ],
        ProvisionedThroughput={
            'ReadCapacityUnits': 5,
            'WriteCapacityUnits': 5
        }
    )

    sleep(2)  # Wait for stream creation
    print('Dynamo table created!')



def create_stream():

    dev_kinesis = boto3.client(
        'kinesis',
        region_name='us-east-1'
    )

    dev_kinesis.create_stream(
        StreamName=KINESIS_STREAM,
        ShardCount=2
    )
    sleep(2)  # Wait for stream creation
    print('Kinesis stream created!')

def delete_stream():
    dev_kinesis = boto3.client(
        'kinesis',
        region_name='us-east-1'
    )
    dev_kinesis.delete_stream(
        StreamName=KINESIS_STREAM,
        EnforceConsumerDeletion=True
    )
    sleep(2)



def get_courier():
    dev_kinesis = boto3.client(
        'kinesis',
        region_name='us-east-1'
    )
    c = Courier('kinesis', stream=KINESIS_STREAM)
    c.courier.client = dev_kinesis
    return c


def get_persister():
    dev_dynamodb = boto3.resource(
        'dynamodb',
        region_name='us-east-1'
    )
    t = dev_dynamodb.Table(KAFKA_DYNAMODB_OFFSETS_TABLE)
    offp = OffsetPersister('dynamodb', KAFKA_TOPIC, region='us-east-1')
    offp.persister.table = t
    return offp


def create_topic():
    a = AdminClient({'bootstrap.servers': 'ec2-54-175-207-94.compute-1.amazonaws.com:9092', 'debug': 'broker,admin'})
    new_topic = [
        NewTopic(KAFKA_TOPIC, num_partitions=1, replication_factor=1)
    ]
    fs = a.create_topics(new_topic)
    for topic, f in fs.items():
        try:
            f.result()
        except Exception as e:
            print(e)
    sleep(2)
    print('Kafka topic created!')


def publish():
    value_schema_str = """
    {
        "type": "record",
        "name": "topicValue",
        "namespace": "com.landoop.topic",
        "fields": [
            {
            "name": "id",
            "type": "string"
            },
            {
            "name": "time",
            "type": "string"
            }
            
        ]
    }
    """
    value_schema = avro.loads(value_schema_str)
    avroProducer = AvroProducer(
        {
            'bootstrap.servers': 'ec2-54-175-207-94.compute-1.amazonaws.com:9092',
            'on_delivery': lambda e, msg: None,
            'schema.registry.url': 'http://ec2-54-175-207-94.compute-1.amazonaws.com:8081'
        },
        default_value_schema=value_schema
    )
    for i in range(25):
        value = {
            "id": str(uuid.uuid4()), 
            "time": str(datetime.now())
        }
        avroProducer.produce(topic=KAFKA_TOPIC, value=value)
    avroProducer.flush()

def prepare_topic():
    a = AdminClient({'bootstrap.servers': 'ec2-54-175-207-94.compute-1.amazonaws.com:9092'})
    topic = [KAFKA_TOPIC]
    fs = a.delete_topics(topic)
    for topic, f in fs.items():
        try:
            f.result()
        except Exception:
            pass




def main(): 
    # publish()   
    # prepare_topic()

    create_topic()

    # logging.basicConfig(level=logging.INFO)

    # def _on_revoke(consumer, partitions):
    #     pass

    # def _on_assign(consumer, partitions):
    #     topic = partitions[0].topic
    #     partition_numbers = [p.partition for p in partitions]
    #     op = offp
    #     loff = op.get_last_offsets(partition_numbers)
    #     consumer.assign(
    #         [TopicPartition(topic, part, offs) for part, offs in loff.items()]
    #     )
    #     print(f'New partition assignment: {loff}')

    # try:
    # #     delete_stream()
    # prepare_topic()
    # #     delete_table()
    # except:
    #     pass
    # # create_table()
    # create_topic()
    # # create_stream()
    # publish()

    # cour = get_courier()
    # offp = get_persister()
    # ac = AvroConsumer(
    #         consumer_conf=consumer_conf,
    #         sr_conf=sr_conf,
    #         topic=KAFKA_TOPIC,
    #         courier=cour,
    #         offset_persister=offp,
    #         value_schema_subject=KAFKA_TOPIC+'-value'
    #     )
    # ac.subscribe(on_assign=_on_assign, on_revoke=_on_revoke)
    # ac.consume()

if __name__ == "__main__":
    main()